
  

# GitTags  
  
Aplicação referente ao desafio da brainn-co  
Para inicial o projeto e necessário ter o NodeJs instalado e o mongodb.  
  
## Configuracoes API  
  
Primeiro acesse a pasta server `cd ./server` e configure seu crie o arquivo `.env`  
passando as variáveis de ambiente conforme segue no arquivo `.env.sample`.  
  
## Instalação de dependências  
  
Rode o comando `npm install` para instalar as dependências  
Caso tenha o yarn instalado basta rodar o comando `yarn`.  
  
## Testes  
Para rodar os testes da API execute o comando `npm test` ou `yarn test`  
  
## Subindo a API  
  
Para subir a API basta rodar o comando `npm start` no caso do yarn `yarn start`  
  
Lembrando que o mongodb deve estar rodando.  
  
## Docker  
Caso queira subir a aplicação no Docker também e possível.  
Nesta opção não e necessário ter o mongodb instalado, somente o docker e o docker-compose.  
  
Rode o comando `bash shell.bash local deploy` dentro da pasta server e o build da aplicação sera realizado.

Lembrado que por padrao a aplicação roda na porta 3000, caso tenha alterado a porta no `.env` devera replicar esta alteracao no Dockerfile e no docker-compose.yaml

## Configuracoes Client

Abra o arquivo `constants.js` que esta no diretorio `client/src/util`
e informe o host e a porta da sua API

## Instalacao de dependencias

Rode o comando `npm install` para instalar as dependencias
Caso tenha o yarn instalado basta rodar o comando `yarn`.

## Subindo a Aplicação

Para subir a aplicação, rode o comando `npm start` ou `yarn start` e a aplicação ira rodar em ambiente de desenvolvimento.

Acesse `http://localhost:3001`

## Docker

Para subir a aplicação no docker basta acessar a pasta `client` e rodar o comando `bash shell.bash local deploy`. Apos o script terminar acesse `http://localhost:80` ou `http://localhost`


